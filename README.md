# OPS Team 1 - WS2015#

Zielbetriebssystem: Windows 7 - 10     
.NET Version: 4.5.2

Das Programm besteht im ganzen aus drei Komponenten:

* Function Container - Dieser enthält die zu berechnenten Funktionen und ihre Ableitungen
* Berechnungsklassen - Für jedes Verfahren wurde eine eigene Klasse angelegt
* GUI - Die Grafische Oberfläche dient zum Eingeben der Daten. Das Ergebnis wird in einem Konsolenfenster ausgegeben.