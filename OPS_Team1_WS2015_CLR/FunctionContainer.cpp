#include "FunctionContainer.h"
#include <cmath>

double FunctionContainer::getFunction(double x, int index) {
	if (index == 0)
		return Function1(x);
	else if (index == 1)
		return Function2(x);
	else if (index == 2)
		return Function3(x);
	else if (index == 3)
		return Function4(x);
}

double FunctionContainer::getDerivatedFunction(double x, int index) {
	if (index == 0)
		return Function1Derivate(x);
	else if (index == 1)
		return Function2Derivate(x);
	else if (index == 2)
		return Function3Derivate(x);
	else if (index == 3)
		return Function4Derivate(x);
}

std::string FunctionContainer::getFunctionString(int index) {
	if (index == 0) {
		return "(x - 30)^2";
	}
	else if (index == 1) {
		return "x^5+5*x^4+5*x^3-5*x^2-6*x";
	}
	else if (index == 2) {
		return "x^4-16*x^2-1";
	}
	else if (index == 3) {
		return "(x^4/4)-x^2+2*x";
	}
	else {
		return "Unbekannter Index!";
	}
}

std::string FunctionContainer::getDerivatedFunctionString(int index) {
	if (index == 0) {
		return "2*(x-30)";
	}
	else if (index == 1) {
		return "5*x^4+20*x^3+15*x^2-10*x-6";
	}
	else if (index == 2) {
		return "4*x^3-32*x";
	}
	else if (index == 3) {
		return "x^3-2*x+2";
	}
	else {
		return "Unbekannter Index!";
	}
}

double FunctionContainer::Function1(double x) {
	return std::pow(x - 30, 2);
}

double FunctionContainer::Function1Derivate(double x) {
	return (2*(x-30));
}

double FunctionContainer::Function2(double x) {
	return std::pow(x,5)+5*std::pow(x,4)+5*std::pow(x,3)-5*std::pow(x,2)-6*x;
}

double FunctionContainer::Function2Derivate(double x) {
	return 5*std::pow(x,4)+20*std::pow(x,3)+15*std::pow(x,2)-10*x-6;
}

double FunctionContainer::Function3(double x) {
	return std::pow(x,4)-16*std::pow(x,2)-1;
}

double FunctionContainer::Function3Derivate(double x) {
	return 4*std::pow(x,3)-32*x;
}

double FunctionContainer::Function4(double x) {
	return std::pow(x, 4)/4 - std::pow(x,2) + 2*x;
}

double FunctionContainer::Function4Derivate(double x) {
	return std::pow(x,3)-2*x+2;
}