#ifndef FIBONACCI_H
#include <iostream>
#include <iomanip>
#define FIBONACCI_H

class Fibonacci
{
public:
	static void minFibonacci(double grenzeU, double grenzeO, unsigned n, int functionIndex);  // Untergrenze, Obergrenze, Anzahl der Suchschritte // Funktion muss noch �bergeben werden
	// Eventuelle Erg�nzung/Erweiterung: �bergeben einer Genauigkeit (Abweichung) anstatt der Suchschritte. Beispiel auf:
	// http://www.maplesoft.com/applications/view.aspx?SID=4193&view=html

private:
	static double fibonacci(unsigned index);
};

#endif