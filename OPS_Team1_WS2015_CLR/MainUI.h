#pragma once
#include "BiSektion.h"
#include "Fibonacci.h"
#include "GoldenerSchnitt.h"

namespace OPS_Team1_WS2015_CLR {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Zusammenfassung f�r MainUI
	/// </summary>
	public ref class MainUI : public System::Windows::Forms::Form
	{
	public:
		MainUI(void)
		{
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			//
		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~MainUI()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::SplitContainer^  splitContainer1;
	protected:
	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::ComboBox^  comboVerfahren;

	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::ListBox^  listBox1;
	private: System::Windows::Forms::Label^  label1;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  inputUntergrenze;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  inputObergrenze;

	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  inputIteration;



	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::TextBox^  inputGenauigkeit;
	private: System::Windows::Forms::TextBox^  log;
	private: System::Windows::Forms::Label^  label6;


	protected:

	protected:

	protected:


	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->splitContainer1 = (gcnew System::Windows::Forms::SplitContainer());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->log = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->inputGenauigkeit = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->inputIteration = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->inputObergrenze = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->inputUntergrenze = (gcnew System::Windows::Forms::TextBox());
			this->comboVerfahren = (gcnew System::Windows::Forms::ComboBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->listBox1 = (gcnew System::Windows::Forms::ListBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->BeginInit();
			this->splitContainer1->Panel1->SuspendLayout();
			this->splitContainer1->Panel2->SuspendLayout();
			this->splitContainer1->SuspendLayout();
			this->panel1->SuspendLayout();
			this->SuspendLayout();
			// 
			// splitContainer1
			// 
			this->splitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->splitContainer1->Location = System::Drawing::Point(0, 0);
			this->splitContainer1->Name = L"splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this->splitContainer1->Panel1->Controls->Add(this->label1);
			this->splitContainer1->Panel1->Controls->Add(this->panel1);
			this->splitContainer1->Panel1->Controls->Add(this->comboVerfahren);
			// 
			// splitContainer1.Panel2
			// 
			this->splitContainer1->Panel2->Controls->Add(this->button1);
			this->splitContainer1->Panel2->Controls->Add(this->listBox1);
			this->splitContainer1->Size = System::Drawing::Size(729, 456);
			this->splitContainer1->SplitterDistance = 365;
			this->splitContainer1->TabIndex = 0;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(58, 431);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(246, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Nichtlineare Optimierung ohne NB - Eindimensional";
			this->label1->Click += gcnew System::EventHandler(this, &MainUI::label1_Click);
			// 
			// panel1
			// 
			this->panel1->Controls->Add(this->label6);
			this->panel1->Controls->Add(this->log);
			this->panel1->Controls->Add(this->label5);
			this->panel1->Controls->Add(this->inputGenauigkeit);
			this->panel1->Controls->Add(this->label4);
			this->panel1->Controls->Add(this->inputIteration);
			this->panel1->Controls->Add(this->label3);
			this->panel1->Controls->Add(this->inputObergrenze);
			this->panel1->Controls->Add(this->label2);
			this->panel1->Controls->Add(this->inputUntergrenze);
			this->panel1->Location = System::Drawing::Point(13, 61);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(335, 367);
			this->panel1->TabIndex = 1;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(134, 220);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(63, 13);
			this->label6->TabIndex = 12;
			this->label6->Text = L"Log-Fenster";
			// 
			// log
			// 
			this->log->CausesValidation = false;
			this->log->Location = System::Drawing::Point(3, 236);
			this->log->Multiline = true;
			this->log->Name = L"log";
			this->log->ReadOnly = true;
			this->log->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->log->Size = System::Drawing::Size(331, 128);
			this->log->TabIndex = 11;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(23, 162);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(64, 13);
			this->label5->TabIndex = 10;
			this->label5->Text = L"Genauigkeit";
			// 
			// inputGenauigkeit
			// 
			this->inputGenauigkeit->Location = System::Drawing::Point(26, 178);
			this->inputGenauigkeit->Name = L"inputGenauigkeit";
			this->inputGenauigkeit->Size = System::Drawing::Size(204, 20);
			this->inputGenauigkeit->TabIndex = 9;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(23, 113);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(57, 13);
			this->label4->TabIndex = 7;
			this->label4->Text = L"Iterationen";
			// 
			// inputIteration
			// 
			this->inputIteration->Location = System::Drawing::Point(26, 129);
			this->inputIteration->Name = L"inputIteration";
			this->inputIteration->Size = System::Drawing::Size(204, 20);
			this->inputIteration->TabIndex = 6;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(23, 64);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(62, 13);
			this->label3->TabIndex = 3;
			this->label3->Text = L"Obergrenze";
			// 
			// inputObergrenze
			// 
			this->inputObergrenze->Location = System::Drawing::Point(26, 80);
			this->inputObergrenze->Name = L"inputObergrenze";
			this->inputObergrenze->Size = System::Drawing::Size(204, 20);
			this->inputObergrenze->TabIndex = 2;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(23, 14);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(65, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Untergrenze";
			// 
			// inputUntergrenze
			// 
			this->inputUntergrenze->Location = System::Drawing::Point(26, 30);
			this->inputUntergrenze->Name = L"inputUntergrenze";
			this->inputUntergrenze->Size = System::Drawing::Size(204, 20);
			this->inputUntergrenze->TabIndex = 0;
			// 
			// comboVerfahren
			// 
			this->comboVerfahren->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboVerfahren->FormattingEnabled = true;
			this->comboVerfahren->Items->AddRange(gcnew cli::array< System::Object^  >(3) {
				L"Fibonacciverfahren", L"Goldener Schnitt",
					L"Bisektionsverfahren"
			});
			this->comboVerfahren->Location = System::Drawing::Point(13, 13);
			this->comboVerfahren->Name = L"comboVerfahren";
			this->comboVerfahren->Size = System::Drawing::Size(335, 21);
			this->comboVerfahren->TabIndex = 0;
			this->comboVerfahren->SelectedIndexChanged += gcnew System::EventHandler(this, &MainUI::comboVerfahren_SelectedIndexChanged);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(4, 388);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(344, 56);
			this->button1->TabIndex = 1;
			this->button1->Text = L"Berechnung Starten";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MainUI::button1_Click);
			// 
			// listBox1
			// 
			this->listBox1->FormattingEnabled = true;
			this->listBox1->Items->AddRange(gcnew cli::array< System::Object^  >(4) {
				L"f(x) = (x-30)^2", L"f(x) = x^5+5*x^4+5*x^3-5*x^2-6*x",
					L"f(x) = x^4-16*x^2-1", L"f(x) = (x^4/4)-x^2+2*x"
			});
			this->listBox1->Location = System::Drawing::Point(4, 13);
			this->listBox1->Name = L"listBox1";
			this->listBox1->Size = System::Drawing::Size(344, 368);
			this->listBox1->TabIndex = 0;
			// 
			// MainUI
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(729, 456);
			this->Controls->Add(this->splitContainer1);
			this->Name = L"MainUI";
			this->Text = L"OPS Team 1 WS2015";
			this->Load += gcnew System::EventHandler(this, &MainUI::MainUI_Load);
			this->splitContainer1->Panel1->ResumeLayout(false);
			this->splitContainer1->Panel1->PerformLayout();
			this->splitContainer1->Panel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitContainer1))->EndInit();
			this->splitContainer1->ResumeLayout(false);
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void MainUI_Load(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void label1_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	public: System::Void createInputFibonacci() {

	}
	//-------------------------------- Element - Methoden -------------------------------------------
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		try {
			int index = this->listBox1->SelectedIndex;
			if (index < 0)
				throw - 1;

			String^ SObergrenze = this->inputObergrenze->Text;
			String^ SUntergrenze = this->inputUntergrenze->Text;
			double  obergrenze = (float)(Convert::ToDouble(SObergrenze));
			double  untergrenze = (float)(Convert::ToDouble(SUntergrenze));

			String^ SIteration;
			String^ SGenauigkeit;
			double iteration, genauigkeit;
			switch (comboVerfahren->SelectedIndex) {
			case 0:
				SIteration = this->inputIteration->Text;
				iteration = (float)(Convert::ToDouble(SIteration));
				Fibonacci::minFibonacci(untergrenze, obergrenze, iteration, index);
				break;
			case 1:
				SIteration = this->inputIteration->Text;
				iteration = (float)(Convert::ToDouble(SIteration));
				GoldenerSchnitt::berechnungGSAB(untergrenze, obergrenze, iteration, index);
				break;
			case 2:
				SGenauigkeit = this->inputGenauigkeit->Text;
				genauigkeit = (float)(Convert::ToDouble(SGenauigkeit));
				BiSektion::nullBisektion(untergrenze, obergrenze, genauigkeit, index);
				break;
			default:
				break;
			}
		}
		catch(System::FormatException ^ e) {
			this->log->Text = "Bitte alle ben�tigten Attribute eingeben !";
		}
		catch (int e) {
			this->log->Text = "Bitte eine Formel zur Berechnung ausw�hlen !";
		}
	}
	private: System::Void comboVerfahren_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		this->inputObergrenze->Enabled = 1;
		this->inputUntergrenze->Enabled = 1;
		this->inputIteration->Enabled = 1;
		this->inputGenauigkeit->Enabled = 1;
		switch (comboVerfahren->SelectedIndex) {
		case 0:
			this->inputGenauigkeit->Enabled = 0;
			break;
		case 1:
			this->inputGenauigkeit->Enabled = 0;
			break;
		case 2:
			this->inputIteration->Enabled = 0;
			break;
		default:
			break;
		}
	}
};
}