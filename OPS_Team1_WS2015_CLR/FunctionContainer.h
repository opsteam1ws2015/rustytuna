#ifndef FUNCTIONCONTAINER_H
#define FUNCTIONCONTAINER_H

#include <string>

class FunctionContainer {
	static double Function1(double);
	static double Function1Derivate(double);
	static double Function2(double);
	static double Function2Derivate(double);
	static double Function3(double);
	static double Function3Derivate(double);
	static double Function4(double);
	static double Function4Derivate(double);
public:
	double static getFunction(double, int);
	double static getDerivatedFunction(double, int);
	std::string static getFunctionString(int);
	std::string static getDerivatedFunctionString(int);

};

#endif
