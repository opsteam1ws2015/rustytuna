/*
	Autor: Andreas Gosch & Philip Klaus
	Datum: 16.10.2015
*/

#include <iostream>
#include "Funktion.h"
#include "BiSektion.h"
#include "Fibonacci.h"
#include "GoldenerSchnitt.h"
#include "MainUI.h"

using namespace std;
using namespace System;
using namespace System::Windows::Forms;

// Verwendete Funktion (Hardcoded) : f(x) = (x-30)^2
// Nichtlineare Optimierung ohne Nebenbedinungen ab Seite 5

[STAThread]
int main() {
	double grenzeU, grenzeO, a, b;
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	OPS_Team1_WS2015_CLR::MainUI form;
	Application::Run(%form);

	return 0;
}