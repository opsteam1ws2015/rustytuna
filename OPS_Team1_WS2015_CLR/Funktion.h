#ifndef FUNKTION_H
#define FUNKTION_H
# include "FunctionContainer.h"

struct Funktion
{
    //1D
    double operator()(double x);
    virtual double value(double x);
    virtual double x(double x);
    virtual double xx(double x);
};

#endif // FUNKTION_H
